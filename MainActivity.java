/**
 * 
 */
package com.sam.activity;

import java.util.ArrayList;
import java.util.List;

import com.sam.R;
import com.sam.fragment.CanBackFragment;
import com.sam.fragment.EvaluationFragment;
import com.sam.fragment.EventFragment;
import com.sam.fragment.IntroductionFragment;
import com.sam.fragment.NewsFragment;
import com.sam.fragment.VideoFragment;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.SlidingDrawer;
import android.widget.TextView;
import android.widget.SlidingDrawer.OnDrawerCloseListener;
import android.widget.SlidingDrawer.OnDrawerOpenListener;

/**
 * @author zapp.sven
 * 
 */
public class MainActivity extends FragmentActivity {
	private final static int TAB_NUM = 5;
	
	private List<TitleInfo> mTitles;
	private List<View> mTabRadios;
	private List<CanBackFragment> mFragments;
	private View mHeadView;
	private SlidingDrawer mSlidingDrawer;
	private int mCurrentTab = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		initInfo();
		prepareTabFragments();
		initViews();
		
		// default show news
		setCurrentTab(0);
	}

	private void initViews() {
		mHeadView = findViewById(R.id.mainhead);
		mSlidingDrawer = (SlidingDrawer) findViewById(R.id.main_sliding);
		mSlidingDrawer.setOnDrawerOpenListener(new OnDrawerOpenListener() {
			@Override
			public void onDrawerOpened() {
				mHeadView.setVisibility(View.VISIBLE);
			}
		});
		mSlidingDrawer.setOnDrawerCloseListener(new OnDrawerCloseListener() {
			@Override
			public void onDrawerClosed() {
				mHeadView.setVisibility(View.GONE);
			}
		});
		
		mTabRadios = new ArrayList<View>();
		View tab1 = findViewById(R.id.item_news);
		mTabRadios.add(tab1);
		View tab2 = findViewById(R.id.item_evaluation);
		mTabRadios.add(tab2);
		View tab3 = findViewById(R.id.item_event);
		mTabRadios.add(tab3);
		View tab4 = findViewById(R.id.item_video);
		mTabRadios.add(tab4);
		View tab5 = findViewById(R.id.item_introduction);
		mTabRadios.add(tab5);
		for (int i = 0; i < mTabRadios.size(); i++) {
			View view = mTabRadios.get(i);
			((TextView) view.findViewById(R.id.tv_head_title)).setText(mTitles.get(i).mainTitle);
			((TextView) view.findViewById(R.id.tv_head_title_sub)).setText(mTitles.get(i).subTitle);
			((ImageView) view.findViewById(R.id.iv_head_icon)).setImageResource(mTitles.get(i).mainIcon);
			// set index for view
			view.setTag(i);
			view.setOnClickListener(mRadioClickListener);
		}
	}
	
	private void initInfo() {
		mTitles = new ArrayList<MainActivity.TitleInfo>();
		mTitles.add(new TitleInfo(R.string.main_navig1, R.string.main_navig1_small, R.drawable.main_edit));
		mTitles.add(new TitleInfo(R.string.main_navig2, R.string.main_navig2_small, R.drawable.main_edit));
		mTitles.add(new TitleInfo(R.string.main_navig3, R.string.main_navig3_small, R.drawable.main_edit));
		mTitles.add(new TitleInfo(R.string.main_navig4, R.string.main_navig4_small, R.drawable.main_edit));
		mTitles.add(new TitleInfo(R.string.main_navig5, R.string.main_navig5_small, R.drawable.main_edit));
	}

	private void prepareTabFragments() {
		mFragments = new ArrayList<CanBackFragment>();
		mFragments.add(new NewsFragment());
		mFragments.add(new EvaluationFragment());
		mFragments.add(new EventFragment());
		mFragments.add(new VideoFragment());
		mFragments.add(new IntroductionFragment());
	}

	private void setCurrentTab(int index) {
		if (index == mCurrentTab)
			return;
		
		mCurrentTab = index;
		// change title info
		TitleInfo titleInfo = mTitles.get(index);
		((TextView)mHeadView.findViewById(R.id.tv_head_title)).setText(titleInfo.mainTitle);
		((TextView)mHeadView.findViewById(R.id.tv_head_title_sub)).setText(titleInfo.subTitle);
		((ImageView)mHeadView.findViewById(R.id.iv_head_icon)).setImageResource(titleInfo.mainIcon);
		// FIXME: should update the number of items

		// change menu items
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		for (int i = 0; i < TAB_NUM; i++) {
			CanBackFragment fragment = mFragments.get(i);
			if (i == index) {
				mTabRadios.get(i).setVisibility(View.GONE);

				if (!fragment.isAdded()) {
					ft.add(R.id.content, fragment);
				}
			} else {
				mTabRadios.get(i).setVisibility(View.VISIBLE);

				if (fragment.isAdded()) {
					ft.remove(fragment);
					ft.addToBackStack(null);
				}
			}
		}
		ft.commit();
	}
	
	@Override
	public void onBackPressed() {
		if (mSlidingDrawer.isOpened()) {
			// sliding is opened
			mSlidingDrawer.animateClose();
			return;
		}else if(mFragments.get(mCurrentTab).canBack()) {
			// current tab child fragment can back
			mFragments.get(mCurrentTab).onBackPressed();
			return;
		}
		// FIXME: after workaround, the back stack will be longer and longer...
		finish();
		
		// do not call default onBackPressed(), cause there maybe a so long
		// back stack for fragments(Sven, 2013/01/26)
		//super.onBackPressed();
	}

	public static class TitleInfo {
		public int mainTitle;
		public int subTitle;
		public int mainIcon;

		public TitleInfo(int mainTitle, int subTitle, int mainIcon) {
			this.mainTitle = mainTitle;
			this.subTitle = subTitle;
			this.mainIcon = mainIcon;
		}
	}

	private OnClickListener mRadioClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// get view index(defined in xml configuration)
			Integer index = (Integer) v.getTag();
			setCurrentTab(index);
			mSlidingDrawer.animateClose();
		}
	};
}
